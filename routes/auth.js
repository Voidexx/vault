const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const database = require('../DBqueries/db');

const urlEncodedParser = bodyParser.urlencoded({extended: true});


// Routing

router.get('/register', (req, res) => res.render('authentication/registration'));

router.get('/login', (req, res) => res.render('authentication/login'));


router.get('/logout', (req, res ) => {
    req.session.destroy((err) =>{
        if(err) {
            console.log(err);
            res.status(500).send('Не удалось убить сессию!');
            return;
        };

        res.locals.loggedin = false;
        res.redirect('/');
    });
});


router.post('/register', urlEncodedParser, (req, res) =>{
    
    const uname = req.body['username'];
    const pass = req.body['password'];
    const rpass = req.body['retype-password'];

    if(pass === rpass) {
        database.checkUserExists(uname, makeUserExistsRegister(req,res,uname,pass));    
    } else {
        res.render('authentication/registration', {flashError: 'Введенные пароли не совпадают!'})
    };
});

router.post('/login', urlEncodedParser, (req, res) => {
    const uname = req.body['username'];
    const pass = req.body['password'];

    database.checkUserExists(uname, makeUserExistsLogin(req, res, uname, pass));
});


// database callback middlewares

const makeUserExistsRegister = (req,res, uname, pass) => {
    return (err, results) => {
        if(err) {
            console.log(err);
            res.status(500),send('Ошибка запроса к БД!');
            return;
        };

        if(results.length > 0) {
            res.render('authentication/registration', {flashError: 'Такой пользователь уже существует!'});
            
        } else {
            database.createNewUser(uname, pass, makeCreateUser(req, res));
        };
    };
};


const makeUserExistsLogin = (req, res, uname, pass) => {
    return (err, results) => {
        if(err) {
            console.log(err);
            res.statur(500).send('Ошибка запроса к БД!');
            return;
        };

        if(results.length > 0) {
            const user = results[0];
            if(req.body['password'] === user['password']) {
                const session = req.session;
                session.loggedin = true;
                session.username = uname;
                session.userid = user['id'];
                req.session.save(err => req.session.reload(err => res.redirect('../post/list')));
            } else {
                res.render('authentication/login', {flashError: 'Введите правильный пароль!'}); 
            };

        } else {
            res.render('authentication/login', {flashError: 'Такого пользователя не существует!'});
        };
    };
};


const makeCreateUser = (req, res) => {
    return (err,results) => {
        if(err) {
            console.log(err);
            res.status(500).send('Не удалось запихать юзера в базу данных!');
            return;
        };

        console.log('Успех!');
    };
};

// Login check middlewares

const pugLoggedInMw = (req, res, next) => {
    res.locals.loggedin = req.session.loggedin || null;
    res.locals.username = req.session.username || null;
    next();
};

const loginCheckMw = (req, res, next) => {
    if(req.session.loggedin) {
        next()
    } else{
        res.status(302).send('Вы должны пройти авторизацию для просмотра этой страницы!');
    };
};

exports.router= router;
exports.pugLoggedInMw = pugLoggedInMw;
exports.loginCheckMw = loginCheckMw; 