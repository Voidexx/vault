const express = require('express');
const router = express.Router();
const auth = require('./auth');
const database = require('../DBqueries/db');
const bodyParser = require('body-parser');
const loginCheckMw = auth.loginCheckMw; 
const methodOverride = require('method-override');

const urlEncodedParser = bodyParser.urlencoded({extended: true});


router.use(methodOverride('_method'));
// Routing

router.use(loginCheckMw);


router.get('/create', (req,res) => res.render('posts/createpost'));

router.get('/list', (req, res) => {
    database.getPosts(20, (err, results) => {
        if(err) {
            console.log(err);
            res.status(500).send('Упс, похоже ошибка!');
            return;
        };
        dateSlicer(results);
        res.render('posts/postlist', {posts: results});
    });
});

router.get('/list/update/:id', (req, res) => {
    const postId = req.params.id;
    database.getUpdatePost(postId, (err, results) => {
        if(err) {
            console.log(err);
            res.status(500).send('Упс, похоже ошибка!');
            return;
        };
        
        res.render('posts/updatepost', {posts: results});
    });
});

router.post('/create', urlEncodedParser, (req, res) => {

    const title = req.body['title'];
    const content = req.body['content'];
    const author = req.session['userid'];
    
    database.createNewPost(title, content, author, newPost(req, res));

});

router.put('/list/update/:id', urlEncodedParser,  (req,res) => {
    
    const postTitle = req.body['title'];
    const postContent = req.body['content'];
    const postId = req.params.id;
    
    database.updatePost(postTitle, postContent, postId, (err, results) => {
        if(err) {
            console.log(err);
            res.status(500).send('Упс, похоже ошибка');
            return;
        };

        res.redirect('/post/list');
    });
});

router.delete('/list/delete/:id', (req, res) => {
    const postId = req.params.id;
    database.deletePost(postId, (err, results) => {
        if(err) {
            console.log(err);
            res.status(500).send('Упс, что-то пошло не так!');
            return;
        };

        res.redirect('/post/list');
    });
});





// this function slices date timezone form database response

const dateSlicer  = (data) => {
    data.forEach(elem => {
        const date = elem.date.toString().slice(0,21);
        elem.date = date;
    });
    return;
};
// database callback middlewares

const newPost = (req, res) => {
    return (err, results) => {
        if(err) {
            console.log(err);
            res.status(500).send('Ошибка запроса к БД!');
            return;
        };

        res.redirect('/post/list');
    };
};

exports.router= router;