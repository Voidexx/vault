const mysql = require('mysql');

const pool = mysql.createPool({
    host: 'localhost',
    user: 'voidx',
    password: 'neverhood91289',
    database: 'blogDB',
    connectionLimit: 10,
    supportBigNumbers: true,
});


exports.createNewUser = (username, password, callback) => {
    const query = 'INSERT INTO users(username, password) VALUES(?,?);';

    queryDataBase(query, [username, password], callback);
    
};

exports.checkUserExists = (username, callback) => {
    const query = 'SELECT * FROM users u WHERE u.username = ?;';

    queryDataBase(query, [username], callback);
};

exports.createNewPost = (title, content, author, callback) => {
    const query = 'INSERT INTO posts(title, content, date, author) VALUES(?, ?, NOW(), ?);';

    queryDataBase(query, [title, content, author], callback);
};

exports.deletePost = (id, callback) => {
    const query = 'DELETE FROM posts WHERE id = ?;';

    queryDataBase(query, [id], callback);
};

exports.getUpdatePost = (id, callback) => {
    const query = 'SELECT * FROM posts WHERE id = ?;';

    queryDataBase(query, [id], callback);
};

exports.updatePost = (title, content, id, callback) => {
    const query = 'UPDATE posts SET title = ?, content = ? WHERE id = ?;';

    queryDataBase(query, [title, content, id], callback);
}; 


exports.getPosts = (count, callback) => {
    const query = 'SELECT title, content, date, p.id, username FROM posts p INNER JOIN users u ON p.author = u.id ORDER BY p.date DESC LIMIT ?;';
    let postscount = count || 10;

    queryDataBase(query, [postscount], callback);
};

exports.getUserPosts = (uname, userid, callback) => {
    const query = 'SELECT username , date, title, content FROM posts p INNER JOIN users u ON  p.author = u.id WHERE u.id = ? ORDER BY date DESC;';

    queryDataBase(query , [userid], callback);
};


//  Database query main function

const queryDataBase = (query, data, callback) => {
    pool.getConnection((err, connection) => {
        if(err) {
            console.log(err);
            callback(err);
            return;
        };

        connection.query(query, data, (err, results) => {
            connection.release();
            if(err) {
                conosle.log(err);
                callback(err);
                return;
            };

        callback(null, results);

        });
    });
};


// SELECT username , date, title, content FROM users u INNER JOIN posts p WHERE  p.author = 1 AND u.id = '1' ORDER BY date DESC;
