const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const auth = require('./routes/auth');
const authRoute = auth.router;
const postRoute = require('./routes/postRouting').router;
const session = require('express-session');
const MySQLStore = require('express-mysql-session');
const pugLoggedInMw = auth.pugLoggedInMw;
const app = express();

const MySQLStoreOptions = {
    host: 'localhost',
    port: 3306,
    user: 'voidx',
    password: 'neverhood91289',
    database: 'blogDB',
    expiration: 9000000000,
    connectionLimit: 10
}; 

const sessionStore = new MySQLStore(MySQLStoreOptions);
const sessionInfo = {
    secret: 'gfvbcbghjrfwf',
    store: sessionStore,
    resave: true,
    saveUninitialized: false,
    cookie: {maxAge:9000000000},
};


app.use(express.static(path.join(__dirname, 'public')));

app.use(session(sessionInfo));

app.set('view engine', 'pug');
app.set('views', './views');

app.get('*', pugLoggedInMw);

app.use('/auth', authRoute);
app.use('/post', postRoute);

app.get('/', (req, res) => {
    res.render('index');
});



app.listen(3000, () => {
    console.log('Server is running');
});